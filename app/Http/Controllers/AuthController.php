<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function form()
    {
        return view('tugas12.signup');
    }
    public function submit(request $request)
    {
        $first = $request['firstname'];
        $last = $request['lastname'];
        return view('tugas12.welcome',compact('first','last'));
    }
}
