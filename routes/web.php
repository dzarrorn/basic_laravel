<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });
//! tugas12
Route::get('/', 'HomeController@home');
Route::get('/SignUp', 'AuthController@form');
Route::post('/Welcome', 'AuthController@submit');
Route::get('/master',function(){
    return view('layout.master');
});
//! tugas13
Route::get('/table',function(){
    return view('layout.master');
});
Route::get('/data-table',function(){
    return view('table.datatable');
});
Route::get('/table',function(){
    return view('table.table');
});

//! tugas15
Route::get('/cast','CastController@index');
Route::get('/cast/create','CastController@create');
Route::post('/cast','CastController@store');
Route::get('/cast/{id}','CastController@show');
Route::get('/cast/{id}/edit','CastController@edit');
Route::put('/cast/{id}','CastController@update');
Route::delete('/cast/{id}','CastController@destroy');