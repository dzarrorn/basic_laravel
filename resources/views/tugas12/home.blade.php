@extends('layout.master')
@section('judul')
    Sanberbook
@endsection
@section('content')
    <section>
      <h1></h1>
      <h2>Social Media Developer Santai Berkualitas</h2>
      <p>Belajar dan Berbagi agar hidup ini semeakin santai berkualitas</p>
    </section>
    <section>
      <div>
        <h3>Benefit Join di SanberBook</h3>
        <ul>
          <li>Mendapatkan motivasi dari sesame developer</li>
          <li>Sharing knowledge dari para mastah Sanber</li>
          <li>Dibuat oleh calon web developer terbaik</li>
        </ul>
      </div>
      <div>
        <h3>Cara Bergabung ke SanberBook</h3>
        <ol>
          <li>Mengunjungi Website ini</li>
          <li>Mendaftar di <a href="/SignUp">Form Sign Up</a></li>
          <li>Selesai!</li>
        </ol>
      </div>
    </section>
@endsection