@extends('layout.master')
@section('judul')
     Sign Up Form
@endsection
@section('content')
    

    <section>
      <h1>Buat Account Baru!</h1>
    </section>
    <section>
      <div>
        <form action="/Welcome"  method="POST">
            @csrf
          <div>
            <p>First name:</p>
            <input type="text" name="firstname" />
            <p>Last name:</p>
            <input type="text" name="lastname" />
          </div>
          <div>
            <p>Gender:</p>
            <input type="radio" name="male" />
            <label for="male">Male</label><br />
            <input type="radio" name="female" />
            <label for="female">Female</label><br />
            <input type="radio" name="other" />
            <label for="other">Other</label><br />
          </div>
          <div>
            <p>Nationality</p>
            <select name="naitionality">
              <option value="indonesia">Indonesia</option>
              <option value="malaysia">Malaysia</option>
              <option value="us">US</option>
              <option value="uk">UK</option>
            </select>
          </div>
          <div>
            <p>Language Spoken</p>
            <input type="checkbox" name="indonesia" />
            <label for="indonesia">Indonesia</label><br />
            <input type="checkbox" name="english" />
            <label for="english">English</label><br />
            <input type="checkbox" name="othercountry" />
            <label for="othercountry">Other</label><br />
          </div>
          <div>
            <p>Bio:</p>
            <textarea name="message" cols="30" rows="10"></textarea>
          </div>

          <br /><br />
          <button type="submit">Sign Up</button>
        </form>
      </div>
    </section>
@endsection
